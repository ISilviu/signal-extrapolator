﻿# Digital Signal Extrapolator
Given a digital signal that has gaps, the subject of this project is to build a sufficiently advanced extrapolation system that would estimate future values of the time series given values from the past.

## Initial documents

- [Audio Signal Extrapolation - Theory And Applications - Ismo Kauppinen and Kari Roth, University of Turku, Finland](https://ant-s4.unibw-hamburg.de/dafx/papers/DAFX02_Kauppinen_Roth_signal_extrapolation.pdf). 
- [Comparison of Various Predictors for Audio Extrapolation - Marco Fink, Martin Holters, Udo Zölzer, Helmut-Schmidt-Universität Hamburg, Germany](http://dafx13.nuim.ie/papers/42.dafx2013_submission_27.pdf).

## Story
After reading the following [question](https://dsp.stackexchange.com/questions/101/how-do-i-extrapolate-a-1d-signal), in which the stated problem was very similar to our problem, the second answer mentioned auto-regressive models which seem the way to go for our problem.

After doing some research, we found out some truly interesting articles:

 - [Autoregression Models for Time Series Forecasting With Python](https://machinelearningmastery.com/autoregression-models-time-series-forecasting-python/)
 - [Deep Autoregressive Models](https://eigenfoo.xyz/deep-autoregressive-models/)
 - [AR NET - a simple autoregressive neural network for time series](https://ai.facebook.com/blog/ar-net-a-simple-autoregressive-neural-network-for-time-series/)
 - [Sine forecasting - medium](https://towardsdatascience.com/using-lstms-to-forecast-time-series-4ab688386b1f)
 - [Data Forecasting Cheatsheet](https://machinelearningmastery.com/time-series-forecasting-methods-in-python-cheat-sheet/)
 - [Time Series Analysis] (http://www.statsoft.com/textbook/time-series-analysis)
 - [An End-to-End Project on Time Series and Forecasting with Python] (https://towardsdatascience.com/an-end-to-end-project-on-time-series-analysis-and-forecasting-with-python-4835e6bf050b)
 - (https://towardsdatascience.com/multivariable-time-series-forecasting-using-stateless-neural-networks-e88afdd5cd82)
### Trying to replicate the ideas from the first article
As expected, an auto regressive model achieves good results for a simple sine wave with maximum amplitude of 100 and frequency of 10 as it is fully correlated. However, for a higher frequency, 50 for instance, the mean squared error increases to 5.956. 

#### Immediate future goals for now:
- test this model for more artificially generated signals
- replicate this in C++ using one of the following proposed solutions:
		- find a library in C++ that provides the same models (just auto regressive models for now)
		- embed Python in C++ 
		- make C++ communicate with Python by using a fast communication protocol

# Interesting articles found today
[18.12.2019]

[https://medium.com/@alexrachnog/deep-learning-the-final-frontier-for-signal-processing-and-time-series-analysis-734307167ad6](https://medium.com/@alexrachnog/deep-learning-the-final-frontier-for-signal-processing-and-time-series-analysis-734307167ad6)

[https://deepmind.com/blog/article/wavenet-generative-model-raw-audio](https://deepmind.com/blog/article/wavenet-generative-model-raw-audio)

[https://github.com/ibab/tensorflow-wavenet](https://github.com/ibab/tensorflow-wavenet)

[04.02.2020]
[https://www.altumintelligence.com/articles/a/Time-Series-Prediction-Using-LSTM-Deep-Neural-Networks](https://www.altumintelligence.com/articles/a/Time-Series-Prediction-Using-LSTM-Deep-Neural-Networks)
[https://github.com/topics/autoregressive-neural-networks](https://github.com/topics/autoregressive-neural-networks)

[https://machinelearningmastery.com/gentle-introduction-autocorrelation-partial-autocorrelation/](https://machinelearningmastery.com/gentle-introduction-autocorrelation-partial-autocorrelation/)

[https://towardsdatascience.com/the-complete-guide-to-time-series-analysis-and-forecasting-70d476bfe775](https://towardsdatascience.com/the-complete-guide-to-time-series-analysis-and-forecasting-70d476bfe775)
