from signalgen import *
import numpy as np
from statsmodels.tsa.ar_model import AR
from statsmodels.tsa.arima_model import ARMA
from statsmodels.tsa.arima_model import ARIMA
from timeit import default_timer as timer
from matplotlib import pyplot as plt
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

def get_predictions_and_model(desired_model, train, test):
    model = desired_model(train)
    start = timer()
    fitted_model = model.fit()
    end = timer()
    fit_time = end - start
    start = timer()
    predictions = fitted_model.predict(start=len(train), end=len(train) + len(test) - 1, dynamic = False)
    end = timer()
    prediction_time = end - start
    return predictions, fitted_model, prediction_time, fit_time 

def test_predictions(predictions, test):
    mae = mean_absolute_error(test, predictions)
    mse = mean_squared_error(test, predictions)
    rmse = np.sqrt(mse)
    print(f'Mean absolute error: {mae}\nRoot mean squared error: {rmse}')
    fig, ax = plt.subplots(ncols = 2, sharex=True, figsize=(8, 4))
    ax[0].set_title('Actual')
    ax[0].plot(test, c='blue')
    ax[0].set_ylabel('Amplitude')
    ax[0].set_xlabel('Time')
    ax[1].set_title('Predicted')
    ax[1].set_ylabel('Amplitude')
    ax[1].set_xlabel('Time')
    ax[1].plot(predictions, c='orange')
    fig.tight_layout()
    plt.show()
    return mae, mse, rmse

def test_last_n_samples(frequency, samples_count:list, x, y):
    out_data = []
    for sample_count in samples_count:
        #print(f'Test for the last {sample_count} samples.')
        train, test = y[1:len(y) - sample_count], y[len(y)-sample_count:]
        predictions, fitted_model, prediction_time, fit_time = get_predictions_and_model(AR, train, test)
        #print(f'Lag: {fitted_model.k_ar}')
        mae, mse, rmse = test_predictions(predictions, test)
        out_data.append([frequency, sample_count, fit_time, prediction_time, mae, mse, rmse])
    return out_data

def test_last_n_samples_freq_amp(freq, amp, samples_count:list, x, y):
    out_data = []
    for sample_count in samples_count:
        #print(f'Test for the last {sample_count} samples.')
        train, test = y[1:len(x) - sample_count], y[len(x)-sample_count:]
        predictions, fitted_model, prediction_time, fit_time = get_predictions_and_model(AR, train, test)
        #print(f'Lag: {fitted_model.k_ar}')
        mae, mse, rmse = test_predictions(predictions, test)
        out_data.append([freq, amp, sample_count, fit_time, prediction_time, mae, mse, rmse])
    return out_data    

def test_different_frequencies_waves(sample_rate, duration_in_seconds, frequencies:list, amplitude, samples_count:list):
    data = sine_wave_different_frequencies(sample_rate, duration_in_seconds, frequencies, amplitude)
    out_data = []
    for index, (x,y) in enumerate(data):
        #print(f'Frequency: {frequencies[index]}')
        frequency_data = test_last_n_samples(frequencies[index], samples_count, x, y)
        for single_array in frequency_data:
            out_data.append(single_array)
    return out_data

def test_different_amplitudes_waves(sample_rate, duration_in_seconds, frequency, amplitudes: list, samples_count:list):
    print(amplitudes)
    data = sine_wave_different_amplitudes(sample_rate, duration_in_seconds, frequency, amplitudes)
    out_data = []
    for index, (x,y) in enumerate(data):
        print(f'Amplitude: {amplitudes[index]}')
        amplitude_data = test_last_n_samples(amplitudes[index], samples_count, x, y)
        for single_array in amplitude_data:
            out_data.append(single_array)
    return out_data

def test_different_frequencies_waves_with_noise(sample_rate, duration_in_seconds, loc, scale, frequencies:list, amplitude, samples_count:list):
    data = sine_wave_different_frequencies_with_noise(sample_rate, duration_in_seconds, frequencies, amplitude, loc, scale)
    out_data = []
    for index, (x,y) in enumerate(data):
        print(f'Frequency: {frequencies[index]}')
        frequency_data = test_last_n_samples(frequencies[index], samples_count, x, y)
        for single_array in frequency_data:
            out_data.append(single_array)
    return out_data

def test_different_frequencies_waves_with_noise_freq_amp(sample_rate, duration_in_seconds, loc, scale, frequencies:list, amplitudes:list, samples_count:list):
    data = sine_wave_different_freq_amp_with_noise(sample_rate, duration_in_seconds, frequencies, amplitudes, loc, scale)
    print(len(data))
    out_data = []
    for inner_data in data:
        x, y, freq, amp = inner_data
        print(f'Frequency :{freq}, Amplitude: {amp}')
        freq_amp_data = test_last_n_samples_freq_amp(freq, amp, samples_count, x, y)
        for single_array in freq_amp_data:
            out_data.append(single_array)
    return out_data

def test_different_frequencies_waves_with_noise_freq_amp2(sample_rate, duration_in_seconds, random_spikes, frequencies:list, amplitudes:list, samples_count:list):
    data = sine_wave_different_freq_amp_with_noise2(sample_rate, duration_in_seconds, frequencies, amplitudes, random_spikes)
    print(len(data))
    out_data = []
    for inner_data in data:
        x, y, freq, amp = inner_data
        print(f'Frequency :{freq}, Amplitude: {amp}')
        freq_amp_data = test_last_n_samples_freq_amp(freq, amp, samples_count, x, y)
        for single_array in freq_amp_data:
            out_data.append(single_array)
    return out_data



def test_different_freq_amp_waves(sample_rate, duration, frequencies:list, amplitudes: list, samples_count: list):
    data = sine_wave_different_freq_amp(sample_rate, duration, frequencies, amplitudes)
    out_data = []
    for inner_data in data:
        x, y, freq, amp = inner_data
        print(f'Frequency :{freq}, Amplitude: {amp}')
        freq_amp_data = test_last_n_samples_freq_amp(freq, amp, samples_count, x, y)
        for single_array in freq_amp_data:
            out_data.append(single_array)
    return out_data