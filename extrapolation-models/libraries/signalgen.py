import numpy as np
import itertools

#region Sine related

def sine_wave(sample_rate, duration_in_seconds,  frequency, amplitude):
    x = np.arange(0, duration_in_seconds, 1 / sample_rate)
    y = amplitude * np.sin(2*np.pi*frequency * x) 
    return x,y

def sine_wave_noisy(sample_rate, duration_in_seconds, frequency, amplitude, loc, scale):
    x = np.arange(0, duration_in_seconds, 1 / sample_rate)
    random_spikes = np.random.normal(loc = loc, scale = scale, size=len(x))
    y = (amplitude * np.sin(2*np.pi*frequency * x)) + random_spikes
    return x,y

def sine_wave_different_frequencies(sample_rate, duration_in_seconds, frequencies:list, amplitude):
    data = []
    for frequency in frequencies:
        x, y = sine_wave(sample_rate, duration_in_seconds, frequency, amplitude)
        data.append((x,y))
    return data

def sine_wave_different_amplitudes(sample_rate, duration_in_seconds, frequency, amplitudes:list):
    data = []
    for amp in amplitudes:
        x, y = sine_wave(sample_rate, duration_in_seconds, frequency, amp)
        data.append((x,y))
    return data

def sine_wave_different_freq_amp(sample_rate, duration_in_seconds, frequencies:list, amplitudes:list):
    data = []
    for freq in frequencies:
        for amp in amplitudes:
            x, y = sine_wave(sample_rate, duration_in_seconds, freq, amp)
            data.append((x,y, freq, amp))
    return data

#endregion

#region Sine-sum related

def sine_sum(duration_in_seconds, sample_rate, freq1, amp1, freq2, amp2):
    x = np.arange(0, duration_in_seconds, 1 / sample_rate)
    sine1 = (amp1 * np.sin(2 * np.pi * freq1 * x))
    sine2 = (amp2 * np.sin(2 * np.pi * freq2 * x))
    y = sine1 + sine2
    return (x, y, sine1, sine2)

def sine_sum_different_frequencies(sample_rate, duration_in_seconds, frequencies_pairs, amplitude1, amplitude2):
    data = []
    for freq1, freq2 in frequencies_pairs:
        x, y, _, _ = sine_sum(duration_in_seconds, sample_rate, freq1, amplitude1, freq2, amplitude2)
        data.append((x,y))
    return data

def sine_sum_different_freq_amp(sample_rate, duration_in_seconds, freq1:list, amp1:list, freq2:list, amp2:list):
    combinations = list(itertools.product(freq1, freq2, amp1, amp2))
    print('Finished generating.')
    data = []
    for (fr1, fr2, am1, am2) in combinations:
        x, y, _, _ = sine_sum(duration_in_seconds, sample_rate, fr1, am1, fr2, am2)
        data.append((x, y, fr1, am1, fr2, am2))
    return data

def sine_wave_different_frequencies_with_noise(sample_rate, duration_in_seconds, frequencies:list, amplitude, loc, scale):
    data = []
    for frequency in frequencies:
        x, y = sine_wave_noisy(sample_rate, duration_in_seconds, frequency, amplitude, loc, scale)
        data.append((x,y))
    return data

def sine_wave_different_freq_amp_with_noise(sample_rate, duration_in_seconds, frequencies:list, amplitudes:list, loc, scale):
    data = []
    for freq in frequencies:
        for amp in amplitudes:
            x, y = sine_wave_noisy(sample_rate, duration_in_seconds, freq, amp, loc, scale)
            data.append((x,y, freq, amp))
    return data

#endregion
