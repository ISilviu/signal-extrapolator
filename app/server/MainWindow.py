import sys
import matplotlib
import random
import threading
import os
import functools
import shutil
from librosa.output import write_wav
matplotlib.use('Qt5Agg')



from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QPushButton, QLabel, QProgressBar
from PyQt5.Qt import QSize, QSettings

import networking as nt
import numpy as np
import matplotlib.pyplot as plt

from scipy.io.wavfile import write
from datetime import datetime
from networking import DigitalSignalServer
from SettingsWindow import SettingsWindow
from StatisticsWindow import StatisticsWindow
from ApplicationSettings import Performance, PlotStyle
from ApplicationSettings import PERFORMANCE_SETTING, PLOT_STYLE_SETTING, SCREENSHOT_PREDICTIONS, SCREENSHOTS_PATH, DELETE_SCREENSHOTS, CREATE_WAV, DELETE_WAV, WAV_FILES_PATH, EXTERNAL_DIRECTORIES_PATHS

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

class MatplotlibCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MatplotlibCanvas, self).__init__(fig)

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setWindowTitle('Visualizer')
        self.define_components()
        self.define_interface()
        self.define_interaction()
        self.define_default_settings()
    
    def define_components(self):
        self.screenshot_fig = plt.Figure()
        self.wav_paths = []
        self.screenshot_ax = self.screenshot_fig.add_subplot(111)
        self.server = DigitalSignalServer()
        self.currentSessionDirectory = ""
        self.settings = QSettings('Visualizer Settings')
        self.currentSettings = {
            'fastestRadioButton' : False,
            'balancedRadioButton' : False,
            'bestAccuracyRadioButton' : True,
            'historyRadioButton' : True,
            'currentWindowRadioButton' : False,
            'screenshotPredictionsCheckBox': True,
            'deleteScreenshotsCheckBox': False,
            'createWavCheckBox': False,
            'deleteWavCheckBox': False,
        }
        self.currentSettingsValues = {
            'thresholdSpinBox' : 0.2,
            'maxLagSpinBox' : 0,
            'anomalyThresholdSpinBox' : 2.0  
        }

    def define_default_settings(self):
        self.settings.setValue(PERFORMANCE_SETTING, Performance.BEST_ACCURACY)
        self.settings.setValue(PLOT_STYLE_SETTING, PlotStyle.HISTORY)
        self.settings.setValue(SCREENSHOT_PREDICTIONS, True)
        self.settings.setValue(DELETE_SCREENSHOTS, False)
        self.settings.setValue(CREATE_WAV, False)
        self.settings.setValue(DELETE_WAV, False)

    def define_interaction(self):
        self.listenButton.clicked.connect(self.server.listen)
        self.server.network_status.connect(self.status_updated)
        self.server.ui_status.connect(self.update_interface)
        self.settingsButton.clicked.connect(self.open_settings_window)
        self.server.save_screenshot.connect(self.save_screenshot)
        self.server.save_wav.connect(self.save_wav)
    
    def open_settings_window(self):
        (accepted, new_settings, state) = SettingsWindow.get_settings(self, self.currentSettings, self.currentSettingsValues)
        if accepted:
            self.currentSettings = state
            self.currentSettingsValues['thresholdSpinBox'] = new_settings['thresholdSpinBox']
            self.server.threshold = new_settings['thresholdSpinBox']
            self.currentSettingsValues['maxLagSpinBox'] = new_settings['maxLagSpinBox']
            self.server.set_max_lag(new_settings['maxLagSpinBox'])
            self.currentSettingsValues['anomalyThresholdSpinBox'] = new_settings['anomalyThresholdSpinBox']
            self.server.anomaly_threshold = new_settings['anomalyThresholdSpinBox']
            for key in new_settings:
                self.settings.setValue(key, new_settings[key])

    
    def status_updated(self, status, message):
        self.currentStatusLabel.setText(message)
        if status is nt.NetworkStatus.RECEIVED_HEADER:
            self.create_current_session_directory()
            self.progressBar.show()
            self.progressBar.setMaximum(int(message))
            self.timer = QtCore.QTimer()
            self.timer.setInterval(20)
            receive_data_callback = functools.partial(self.server.receive_data, performance_strategy=self.settings.value(PERFORMANCE_SETTING), save_screenshots = self.settings.value(SCREENSHOT_PREDICTIONS))
            self.timer.timeout.connect(receive_data_callback)
            self.timer.start()
        elif status is nt.NetworkStatus.ENDED:
            self.timer.stop()
            if len(self.server.metrics_holder.fit_times) > 0:
                _ = StatisticsWindow.get_statistics(self, self.server.metrics_holder)
            if self.settings.value(PLOT_STYLE_SETTING) is PlotStyle.CURRENT_WINDOW:
                self.update_plot(self.server.x, self.server.y, self.server.packet_size, PlotStyle.HISTORY)
            self.server.reset()
        elif status is nt.NetworkStatus.ERROR:
            self.timer.stop()
            self.progressBar.setVisible(False)
            self.canvas.axes.cla()
            self.canvas.draw()
            self.server.reset()

    def define_interface(self):
        self.centralWidget = QtWidgets.QWidget()
        self.setCentralWidget(self.centralWidget)

        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralWidget)
        self.buttonsWidget = QtWidgets.QWidget()
        self.buttonsWidgetLayout = QtWidgets.QHBoxLayout(self.buttonsWidget)

        self.listenButton = QPushButton("Listen")
        self.listenButton.setFixedSize(QSize(70,20))

        self.settingsButton = QPushButton("Settings")
        self.settingsButton.setFixedSize(QSize(70, 20))

        self.currentStatusLabel = QLabel()

        self.progressBar = QProgressBar()
        self.progressBar.setFixedSize(QSize(100, 20))
        self.progressBar.hide()
        
        self.buttonsWidgetLayout.addWidget(self.listenButton)
        self.buttonsWidgetLayout.addWidget(self.currentStatusLabel)
        self.buttonsWidgetLayout.addWidget(self.settingsButton)
        self.buttonsWidgetLayout.addWidget(self.progressBar)


        self.canvas = MatplotlibCanvas(self, width=8, height=4, dpi=100)
        toolbar = NavigationToolbar(self.canvas, self)

        self.verticalLayout.addWidget(toolbar)
        self.verticalLayout.addWidget(self.canvas)
        self.verticalLayout.addWidget(self.buttonsWidget)
    
    def update_plot(self, x: np.ndarray, y: np.ndarray, packet_size: int, plot_style: PlotStyle):
        self.canvas.axes.cla()
        if plot_style is PlotStyle.HISTORY:
            self.canvas.axes.plot(x[:len(y)], y , 'r')
        elif plot_style is PlotStyle.CURRENT_WINDOW:
            self.canvas.axes.plot(x[len(y)-packet_size:len(y)], y[-packet_size:] , 'r')
        self.canvas.draw()
    
    def update_interface(self, packets_received, packet_size):
        self.progressBar.setValue(packets_received)
        x = self.server.x
        y = self.server.y
        plotStyleSetting = self.settings.value(PLOT_STYLE_SETTING)
        self.update_plot(x, y, packet_size, plotStyleSetting)

    def save_screenshot(self, x: np.ndarray, y: np.ndarray, predictions: np.ndarray, packets_received: int):
        self.screenshot_ax.cla()
        self.screenshot_ax.plot(x, y, c = 'b')
        self.screenshot_ax.plot(x, predictions, c = 'r')
        self.screenshot_fig.savefig(f'{self.currentSessionDirectory}/{packets_received}.png')

    def save_wav(self, sample_rate: int, y: list):
        if self.settings.value(CREATE_WAV) == 'true':
            self.wav_paths.append(f'{WAV_FILES_PATH}/{datetime.now().strftime("%d-%m-%Y_%H-%M-%S")}.wav')
            write_wav(self.wav_paths[-1], y[0], sample_rate)
            self.wav_paths.append(f'{WAV_FILES_PATH}/{datetime.now().strftime("%d-%m-%Y_%H-%M-%S")}_zeros.wav')
            write_wav(self.wav_paths[-1], y[1], sample_rate)

    def closeEvent(self, event):
        if self.settings.value(DELETE_SCREENSHOTS) == 'true':
            for entry in os.scandir(SCREENSHOTS_PATH):
                try:
                    shutil.rmtree(entry.path)
                except OSError as e:
                    print(str(e))
        if self.settings.value(CREATE_WAV) == 'true' and self.settings.value(DELETE_WAV) == 'true':
            try:
                for wav_path in self.wav_paths:
                    os.remove(wav_path)
            except OSError as e:
                print(str(e))
        event.accept()
    
    def create_current_session_directory(self):
        current_date_time = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
        try:
            path = f'{SCREENSHOTS_PATH}/session_{current_date_time}'
            self.currentSessionDirectory = path
            os.mkdir(path)
        except OSError as e:
            print(str(e))




if __name__ == '__main__':
    for path in EXTERNAL_DIRECTORIES_PATHS:
        if os.path.isdir(path) == False:
            try:
                os.mkdir(path)
            except OSError as e:
                print(str(e))
                sys.exit()

    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    app.exec_()