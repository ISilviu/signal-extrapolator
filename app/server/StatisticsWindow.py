
from PyQt5 import QtCore, QtGui, QtWidgets

from metrics import MetricsHolder
import statistics


class StatisticsWindow(object):
    def define_interface(self, Form, metrics_holder: MetricsHolder):
        Form.setObjectName("Form")
        Form.resize(198, 127)
        self.formLayoutWidget = QtWidgets.QWidget(Form)
        self.formLayoutWidget.setGeometry(QtCore.QRect(10, 10, 181, 112))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setRowWrapPolicy(QtWidgets.QFormLayout.DontWrapRows)
        self.formLayout.setLabelAlignment(QtCore.Qt.AlignCenter)
        self.formLayout.setFormAlignment(QtCore.Qt.AlignCenter)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.fitTimesLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.fitTimesLabel.setObjectName("fitTimesLabel")
        self.verticalLayout_2.addWidget(self.fitTimesLabel, 0, QtCore.Qt.AlignHCenter)
        self.predictionTimesLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.predictionTimesLabel.setObjectName("predictionTimesLabel")
        self.verticalLayout_2.addWidget(self.predictionTimesLabel, 0, QtCore.Qt.AlignHCenter)
        self.maeLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.maeLabel.setObjectName("maeLabel")
        self.verticalLayout_2.addWidget(self.maeLabel, 0, QtCore.Qt.AlignHCenter)
        self.rmseLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.rmseLabel.setObjectName("rmseLabel")
        self.verticalLayout_2.addWidget(self.rmseLabel, 0, QtCore.Qt.AlignHCenter)
        self.noPredictionsLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.noPredictionsLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.noPredictionsLabel.setObjectName("noPredictionsLabel")
        self.verticalLayout_2.addWidget(self.noPredictionsLabel)
        self.formLayout.setLayout(0, QtWidgets.QFormLayout.FieldRole, self.verticalLayout_2)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.formLayoutWidget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label, 0, QtCore.Qt.AlignHCenter)
        self.label_2 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2, 0, QtCore.Qt.AlignHCenter)
        self.label_3 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3, 0, QtCore.Qt.AlignHCenter)
        self.label_4 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_4.setObjectName("label_4")
        self.verticalLayout.addWidget(self.label_4, 0, QtCore.Qt.AlignHCenter)
        self.label_5 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName("label_5")
        self.verticalLayout.addWidget(self.label_5)
        self.formLayout.setLayout(0, QtWidgets.QFormLayout.LabelRole, self.verticalLayout)

        self.retranslateUi(Form)

        self.maeLabel.setText(str(round(statistics.mean(metrics_holder.mae_errors), 3)))
        self.rmseLabel.setText(str(round(statistics.mean(metrics_holder.rmse_errors), 3)))
        self.fitTimesLabel.setText(str(round(statistics.mean(metrics_holder.fit_times), 3)))
        self.predictionTimesLabel.setText(str(round(statistics.mean(metrics_holder.prediction_times),3)))
        self.noPredictionsLabel.setText(str(metrics_holder.blank_predictions))

        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Stats"))
        self.fitTimesLabel.setText(_translate("Form", "TextLabel"))
        self.predictionTimesLabel.setText(_translate("Form", "TextLabel"))
        self.maeLabel.setText(_translate("Form", "TextLabel"))
        self.rmseLabel.setText(_translate("Form", "TextLabel"))
        self.label.setText(_translate("Form", "Fit times:"))
        self.label_2.setText(_translate("Form", "Prediction times:"))
        self.label_3.setText(_translate("Form", "MAE:"))
        self.label_4.setText(_translate("Form", "RMSE:"))
        self.label_5.setText(_translate("Form", "No-predictions:"))

    @staticmethod
    def get_statistics(parent, metrics_holder: MetricsHolder):
        dialog = QtWidgets.QDialog(parent)
        dialog.ui = StatisticsWindow()
        dialog.ui.define_interface(dialog, metrics_holder)
        dialog.setFixedSize(dialog.size())
        result = dialog.exec_()
        return (result == QtWidgets.QDialog.Accepted)