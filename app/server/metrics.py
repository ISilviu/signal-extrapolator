import numpy as np
from sklearn.metrics import mean_absolute_error 
from sklearn.metrics import mean_squared_error

def compute_errors(actual: np.ndarray, predicted: np.ndarray):
    mae = mean_absolute_error(actual, predicted)
    mse = mean_squared_error(actual, predicted)
    rmse = np.sqrt(mse)
    return mae, rmse

class MetricsHolder:
    mae_errors = []
    rmse_errors = []
    fit_times = []
    prediction_times = []
    blank_predictions = 0
