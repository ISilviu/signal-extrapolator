from enum import Enum

PERFORMANCE_SETTING = 'Performance'
PLOT_STYLE_SETTING = 'PlotStyle'
SCREENSHOT_PREDICTIONS = 'ScreenshotPredictions'
DELETE_SCREENSHOTS = 'DeleteScreenshots'
CREATE_WAV = 'CreateWav'
DELETE_WAV = 'DeleteWav'
SCREENSHOTS_PATH = './screenshots'
WAV_FILES_PATH = './wav'
EXTERNAL_DIRECTORIES_PATHS = [SCREENSHOTS_PATH, WAV_FILES_PATH]

class Performance(Enum):
    FASTEST = 0
    BALANCED = 1
    BEST_ACCURACY = 2

class PlotStyle(Enum):
    HISTORY = 0
    CURRENT_WINDOW = 1