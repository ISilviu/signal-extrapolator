from PyQt5 import QtCore, QtGui, QtWidgets

from ApplicationSettings import Performance, PlotStyle
from ApplicationSettings import PERFORMANCE_SETTING, PLOT_STYLE_SETTING, SCREENSHOT_PREDICTIONS, DELETE_SCREENSHOTS, CREATE_WAV, DELETE_WAV

class SettingsWindow(object):
    def define_interface(self, Dialog, defaultSettings, defaultSettingsValues):
        Dialog.setObjectName("Dialog")
        Dialog.resize(612, 376)
        Dialog.setBaseSize(QtCore.QSize(0, 0))
        self.gridLayoutWidget = QtWidgets.QWidget(Dialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 431, 361))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label_4 = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.label_4.setFont(font)
        self.label_4.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_4.addWidget(self.label_4)
        self.label_5 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_5.setWordWrap(True)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_4.addWidget(self.label_5)
        spacerItem = QtWidgets.QSpacerItem(20, 55, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem)
        self.label_10 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_10.setWordWrap(True)
        self.label_10.setObjectName("label_10")
        self.verticalLayout_4.addWidget(self.label_10)
        spacerItem1 = QtWidgets.QSpacerItem(20, 15, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem1)
        self.label_6 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_6.setWordWrap(True)
        self.label_6.setObjectName("label_6")
        self.verticalLayout_4.addWidget(self.label_6)
        spacerItem2 = QtWidgets.QSpacerItem(20, 33, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem2)
        self.label_8 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_8.setWordWrap(True)
        self.label_8.setObjectName("label_8")
        self.verticalLayout_4.addWidget(self.label_8)
        spacerItem3 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_4.addItem(spacerItem3)
        self.gridLayout.addLayout(self.verticalLayout_4, 1, 1, 1, 1)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.groupBox_2 = QtWidgets.QGroupBox(self.gridLayoutWidget)
        self.groupBox_2.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.groupBox_2.setFlat(False)
        self.groupBox_2.setCheckable(False)
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.groupBox_2)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 20, 201, 149))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.fastestRadioButton = QtWidgets.QRadioButton(self.horizontalLayoutWidget)
        self.fastestRadioButton.setObjectName("fastestRadioButton")
        self.verticalLayout_2.addWidget(self.fastestRadioButton)
        self.balancedRadioButton = QtWidgets.QRadioButton(self.horizontalLayoutWidget)
        self.balancedRadioButton.setObjectName("balancedRadioButton")
        self.verticalLayout_2.addWidget(self.balancedRadioButton)
        self.bestAccuracyRadioButton = QtWidgets.QRadioButton(self.horizontalLayoutWidget)
        self.bestAccuracyRadioButton.setObjectName("bestAccuracyRadioButton")
        self.verticalLayout_2.addWidget(self.bestAccuracyRadioButton)
        self.label_9 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_9.setObjectName("label_9")
        self.verticalLayout_2.addWidget(self.label_9)
        self.maxLagSpinBox = QtWidgets.QDoubleSpinBox(self.horizontalLayoutWidget)
        self.maxLagSpinBox.setDecimals(0)
        self.maxLagSpinBox.setMaximum(250.0)
        self.maxLagSpinBox.setSingleStep(1.0)
        self.maxLagSpinBox.setStepType(QtWidgets.QAbstractSpinBox.DefaultStepType)
        self.maxLagSpinBox.setObjectName("maxLagSpinBox")
        self.verticalLayout_2.addWidget(self.maxLagSpinBox)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_8 = QtWidgets.QVBoxLayout()
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        spacerItem4 = QtWidgets.QSpacerItem(17, 110, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout_8.addItem(spacerItem4)
        self.line = QtWidgets.QFrame(self.horizontalLayoutWidget)
        self.line.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout_8.addWidget(self.line)
        self.horizontalLayout.addLayout(self.verticalLayout_8)
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        spacerItem5 = QtWidgets.QSpacerItem(20, 100, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout_7.addItem(spacerItem5)
        self.label_11 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_11.setWordWrap(True)
        self.label_11.setObjectName("label_11")
        self.verticalLayout_7.addWidget(self.label_11)
        self.anomalyThresholdSpinBox = QtWidgets.QDoubleSpinBox(self.horizontalLayoutWidget)
        self.anomalyThresholdSpinBox.setDecimals(1)
        self.anomalyThresholdSpinBox.setObjectName("anomalyThresholdSpinBox")
        self.verticalLayout_7.addWidget(self.anomalyThresholdSpinBox)
        self.horizontalLayout.addLayout(self.verticalLayout_7)
        self.verticalLayout_3.addWidget(self.groupBox_2)
        spacerItem6 = QtWidgets.QSpacerItem(20, 8, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout_3.addItem(spacerItem6)
        self.groupBox = QtWidgets.QGroupBox(self.gridLayoutWidget)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.groupBox)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 20, 103, 61))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.historyRadioButton = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.historyRadioButton.setObjectName("historyRadioButton")
        self.verticalLayout.addWidget(self.historyRadioButton)
        self.currentWindowRadioButton = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.currentWindowRadioButton.setObjectName("currentWindowRadioButton")
        self.verticalLayout.addWidget(self.currentWindowRadioButton)
        self.screenshotPredictionsCheckBox = QtWidgets.QCheckBox(self.groupBox)
        self.screenshotPredictionsCheckBox.setGeometry(QtCore.QRect(10, 90, 141, 17))
        self.screenshotPredictionsCheckBox.setObjectName("screenshotPredictionsCheckBox")
        self.deleteScreenshotsCheckBox = QtWidgets.QCheckBox(self.groupBox)
        self.deleteScreenshotsCheckBox.setGeometry(QtCore.QRect(10, 110, 131, 17))
        self.deleteScreenshotsCheckBox.setObjectName("deleteScreenshotsCheckBox")
        self.verticalLayout_3.addWidget(self.groupBox)
        self.gridLayout.addLayout(self.verticalLayout_3, 1, 0, 1, 1)
        self.verticalLayoutWidget_3 = QtWidgets.QWidget(Dialog)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(450, 10, 158, 361))
        self.verticalLayoutWidget_3.setObjectName("verticalLayoutWidget_3")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.groupBox_3 = QtWidgets.QGroupBox(self.verticalLayoutWidget_3)
        self.groupBox_3.setObjectName("groupBox_3")
        self.verticalLayoutWidget_5 = QtWidgets.QWidget(self.groupBox_3)
        self.verticalLayoutWidget_5.setGeometry(QtCore.QRect(10, 20, 81, 51))
        self.verticalLayoutWidget_5.setObjectName("verticalLayoutWidget_5")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_5)
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_7 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_7.setObjectName("label_7")
        self.verticalLayout_5.addWidget(self.label_7)
        self.thresholdSpinBox = QtWidgets.QDoubleSpinBox(self.verticalLayoutWidget_5)
        self.thresholdSpinBox.setDecimals(1)
        self.thresholdSpinBox.setMaximum(1.0)
        self.thresholdSpinBox.setSingleStep(0.1)
        self.thresholdSpinBox.setObjectName("thresholdSpinBox")
        self.verticalLayout_5.addWidget(self.thresholdSpinBox)
        self.verticalLayout_6.addWidget(self.groupBox_3)
        self.label_3 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_3.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_3.setWordWrap(True)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_6.addWidget(self.label_3)
        self.groupBox_4 = QtWidgets.QGroupBox(self.verticalLayoutWidget_3)
        self.groupBox_4.setObjectName("groupBox_4")
        self.createWavCheckBox = QtWidgets.QCheckBox(self.groupBox_4)
        self.createWavCheckBox.setGeometry(QtCore.QRect(10, 20, 91, 17))
        self.createWavCheckBox.setObjectName("createWavCheckBox")
        self.deleteWavCheckBox = QtWidgets.QCheckBox(self.groupBox_4)
        self.deleteWavCheckBox.setGeometry(QtCore.QRect(10, 40, 91, 17))
        self.deleteWavCheckBox.setObjectName("deleteWavCheckBox")
        self.verticalLayout_6.addWidget(self.groupBox_4)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.verticalLayoutWidget_3)
        self.buttonBox.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.buttonBox.setAutoFillBackground(False)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout_6.addWidget(self.buttonBox)

        self.group_inputs()
        self.group_inputs_by_name()
        self.apply_default_settings(defaultSettings, defaultSettingsValues)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Settings"))
        self.label_4.setText(_translate("Dialog", "Notes"))
        self.label_5.setText(_translate("Dialog", "Choosing the fastest performance option increases the chance of inaccurate predictions."))
        self.label_10.setText(_translate("Dialog", "Changes the maximum lag to be taken into account by the optimizers. When 0, it is automatically computed. When increasing the max lag, anomalies may appear."))
        self.label_6.setText(_translate("Dialog", "The history option plots all the received signal, while current window only plots the current packet of samples."))
        self.label_8.setText(_translate("Dialog", "Deletes the screenshots when closing the application."))
        self.groupBox_2.setTitle(_translate("Dialog", "Prediction"))
        self.label_2.setText(_translate("Dialog", "Performance"))
        self.fastestRadioButton.setText(_translate("Dialog", "Fastest"))
        self.balancedRadioButton.setText(_translate("Dialog", "Balanced"))
        self.bestAccuracyRadioButton.setText(_translate("Dialog", "Best accuracy"))
        self.label_9.setText(_translate("Dialog", "Max lag"))
        self.label_11.setText(_translate("Dialog", "Anomaly threshold"))
        self.groupBox.setTitle(_translate("Dialog", "Visualization"))
        self.label.setText(_translate("Dialog", "Plot style"))
        self.historyRadioButton.setText(_translate("Dialog", "History"))
        self.currentWindowRadioButton.setText(_translate("Dialog", "Current window"))
        self.screenshotPredictionsCheckBox.setText(_translate("Dialog", "Screenshot predictions"))
        self.deleteScreenshotsCheckBox.setText(_translate("Dialog", "Delete screenshots"))
        self.groupBox_3.setTitle(_translate("Dialog", "Packet loss"))
        self.label_7.setText(_translate("Dialog", "Threshold"))
        self.label_3.setText(_translate("Dialog", "A threshold of 0.5 will yield almost equal lost packets to received packets. A lower threshold will yield less lost packets."))
        self.groupBox_4.setTitle(_translate("Dialog", "Output"))
        self.createWavCheckBox.setText(_translate("Dialog", "Create WAV"))
        self.deleteWavCheckBox.setText(_translate("Dialog", "Delete WAV"))

    def group_inputs(self):
        self.plotSyleNameToOption = {
            'historyRadioButton' : PlotStyle.HISTORY,
            'currentWindowRadioButton' : PlotStyle.CURRENT_WINDOW 
        }
        self.plotStyleRadioButtons = [self.historyRadioButton, self.currentWindowRadioButton]

        self.performanceNameToOption = {
            'fastestRadioButton' : Performance.FASTEST,
            'balancedRadioButton' : Performance.BALANCED,
            'bestAccuracyRadioButton' : Performance.BEST_ACCURACY
        }
        self.performanceRadioButtons = [self.balancedRadioButton, self.bestAccuracyRadioButton, self.fastestRadioButton]
    
    def group_inputs_by_name(self):
        self.allInputs = {
            'fastestRadioButton' : self.fastestRadioButton,
            'balancedRadioButton' : self.balancedRadioButton,
            'bestAccuracyRadioButton' : self.bestAccuracyRadioButton,
            'historyRadioButton' : self.historyRadioButton,
            'currentWindowRadioButton' : self.currentWindowRadioButton,
            'screenshotPredictionsCheckBox': self.screenshotPredictionsCheckBox,
            'deleteScreenshotsCheckBox': self.deleteScreenshotsCheckBox,
            'thresholdSpinBox' : self.thresholdSpinBox,
            'maxLagSpinBox' : self.maxLagSpinBox,
            'anomalyThresholdSpinBox' : self.anomalyThresholdSpinBox,
            'createWavCheckBox' : self.createWavCheckBox,
            'deleteWavCheckBox' : self.deleteWavCheckBox
        }

    def apply_default_settings(self, settings, settingsValues):
        for settingName in settings:
            self.allInputs[settingName].setChecked(settings[settingName])
        for settingName in settingsValues:
            self.allInputs[settingName].setValue(settingsValues[settingName])
      
    def collect_inputs(self):
        settings = {}
        state = {}
        for plotStyle in self.plotStyleRadioButtons:
            state[plotStyle.objectName()] = plotStyle.isChecked()
            if plotStyle.isChecked():
                settings[PLOT_STYLE_SETTING] = self.plotSyleNameToOption[plotStyle.objectName()]
                break
        for performanceOption in self.performanceRadioButtons:
            state[performanceOption.objectName()] = performanceOption.isChecked()
            if performanceOption.isChecked():
                settings[PERFORMANCE_SETTING] = self.performanceNameToOption[performanceOption.objectName()]
                break
        screenshot_predictions = self.screenshotPredictionsCheckBox.isChecked()
        delete_screenshots = self.deleteScreenshotsCheckBox.isChecked()

        create_wav = self.createWavCheckBox.isChecked()
        delete_wav = self.deleteWavCheckBox.isChecked()

        state[self.screenshotPredictionsCheckBox.objectName()] = screenshot_predictions
        settings[SCREENSHOT_PREDICTIONS] = screenshot_predictions
        state[self.deleteScreenshotsCheckBox.objectName()] = delete_screenshots
        settings[DELETE_SCREENSHOTS] = delete_screenshots
        state[self.createWavCheckBox.objectName()] = create_wav
        settings[CREATE_WAV] = create_wav
        state[self.deleteWavCheckBox.objectName()] = delete_wav
        settings[DELETE_WAV] = delete_wav

        settings[self.thresholdSpinBox.objectName()] = self.thresholdSpinBox.value()
        settings[self.maxLagSpinBox.objectName()] = self.maxLagSpinBox.value()
        settings[self.anomalyThresholdSpinBox.objectName()] = self.anomalyThresholdSpinBox.value()
        return settings, state


    @staticmethod
    def get_settings(parent = None, defaultSettings = None, defaultSettingsValues = None):
        dialog = QtWidgets.QDialog(parent)
        dialog.ui = SettingsWindow()
        dialog.ui.define_interface(dialog, defaultSettings, defaultSettingsValues)
        dialog.setFixedSize(dialog.size())
        result = dialog.exec_()
        (settings, state) = dialog.ui.collect_inputs()
        return (result == QtWidgets.QDialog.Accepted, settings, state)