from numpy.random import uniform

class RandomDecisionTaker:

    def __init__(self, min, max, needed_samples):
        self.random_samples = uniform(min, max, needed_samples + 1)
    
    def get_decision(self, index, threshold) -> bool:
        return self.random_samples[index] <= threshold
 