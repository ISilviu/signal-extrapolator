import numpy as np
import itertools
from statsmodels.tsa.ar_model import AR
from queue import PriorityQueue
from timeit import default_timer as timer
from sklearn.metrics import mean_absolute_error 
from sklearn.metrics import mean_squared_error
from metrics import compute_errors

from ApplicationSettings import Performance

def predict(aperture: int, optimizer: str, available_data: np.ndarray, before_data: np.ndarray, desired_samples_count: int, maxlag: int = None):
    assert desired_samples_count == len(before_data)
    max_aperture_data = available_data[-aperture:]
    model = AR(max_aperture_data)
    start = timer()
    if maxlag is None:
        model_fit = model.fit(ic=optimizer, maxlag=maxlag)
    else:
        model_fit = model.fit(ic=optimizer, maxlag=int(maxlag))
    end = timer()
    fit_time = end - start
    start = timer()
    predictions = model_fit.predict(start=len(max_aperture_data), end=len(max_aperture_data) + desired_samples_count - 1, dynamic = False)
    end = timer()
    prediction_time = end - start
    mae, rmse = compute_errors(before_data, predictions)
    return predictions, mae, rmse, fit_time, prediction_time

class SignalPredictor:

    def __init__(self, packet_size: int):
        self.packet_size = packet_size
        self.max_value = 0.0
        self.min_value = 0.0
        self.available_optimizers = ['aic', 'bic']
        self.maxlags = [None, ]
        self.statistical_data = {}
        self.available_apertures = {
            Performance.FASTEST : list(range(1152, 127, -128)),
            Performance.BALANCED: list(range(3072, -1, -256)),
            Performance.BEST_ACCURACY: list(range(3072, -1, -256))
        }
        self.performance_strategy_to_function = {
            Performance.BALANCED : self.predict_balanced,
            Performance.BEST_ACCURACY: self.predict_best_accuracy,
            Performance.FASTEST: self.predict_fastest
        }
    
    def predict(self, available_data: np.ndarray, before_data: np.ndarray, performance_strategy : Performance, desired_samples_count: int, maxlag: int = None, anomaly_threshold: float = 0.0):
        if len(available_data) == 0 or np.isnan(available_data).all():
            return np.array([]), 0, 0
        else:
            # reject anomalies
            predictions, fit_times, prediction_times = self.performance_strategy_to_function[performance_strategy](available_data, before_data, desired_samples_count, maxlag)
            if((self.max_value > 0 and np.max(predictions) > self.max_value + anomaly_threshold) or (self.min_value < 0 and np.min(predictions) < self.min_value - anomaly_threshold)):
                predictions = before_data[:]
            return predictions, fit_times, prediction_times 

    def predict_fastest(self, available_data: np.ndarray, before_data: np.ndarray, desired_samples_count: int, maxlag: int):
        assert desired_samples_count == len(before_data)
        start = timer()
        max_aperture = self.__get_max_aperture(len(available_data))
        if max_aperture is not None:
            predictions, _, _, fit_time, prediction_time = predict(max_aperture, 'aic', available_data, before_data, desired_samples_count, maxlag)
            end = timer()
            print(f'Elapsed time: {end - start}.')
            return predictions, fit_time, prediction_time
        else:
            return np.array([]), 0, 0
        
    def predict_best_accuracy(self, available_data: np.ndarray, before_data: np.ndarray, desired_samples_count: int, maxlag: int):
        assert desired_samples_count == len(before_data)
        performances = []
        fit_times = []
        prediction_times = []
        start = timer()
        for max_aperture in self.__get_max_apertures(2, len(available_data)):
            for optimizer in self.available_optimizers:
                predictions, _, rmse, fit_time, prediction_time = predict(max_aperture, optimizer, available_data, before_data, desired_samples_count, maxlag)
                fit_times.append(fit_time)
                prediction_times.append(prediction_time)
                performances.append((rmse, predictions))
        best_predictions = min(performances, key = lambda t: t[0])[1]
        end = timer()
        print(f'Elapsed time: {end - start}.')
        return best_predictions, np.mean(fit_times), np.mean(prediction_times)
    
    def predict_balanced(self, available_data: np.ndarray, before_data: np.ndarray, desired_samples_count: int, maxlag: int):
        assert desired_samples_count == len(before_data)
        performances = []
        fit_times = []
        prediction_times = []
        start = timer()
        for max_aperture in self.__get_max_apertures(2, len(available_data)):
            predictions, _, rmse, fit_time, prediction_time = predict(max_aperture, 'aic', available_data, before_data, desired_samples_count, maxlag)
            fit_times.append(fit_time)
            prediction_times.append(prediction_time)
            performances.append((rmse, predictions))
        best_predictions = min(performances, key = lambda t: t[0])[1]
        end = timer()
        print(f'Elapsed time: {end - start}.')
        return best_predictions, np.mean(fit_times), np.mean(prediction_times)
        
    def __get_max_apertures(self, count: int, data_count: int) -> list:
        current_count = 0
        for aperture in self.available_apertures[Performance.BALANCED]:
            if aperture <= data_count and current_count < count:
                current_count += 1
                yield aperture

    def __get_max_aperture(self, data_count: int) -> int:
        return next((aperture for aperture in self.available_apertures[Performance.FASTEST] if aperture <= data_count), None)




