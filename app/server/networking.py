import socket
import threading
import time
import numpy as np
import dsp
import struct
import matplotlib.pyplot as plt

from enum import Enum
from PyQt5.QtCore import pyqtSignal, QObject
from randomness import RandomDecisionTaker
from metrics import compute_errors
from metrics import MetricsHolder
from SignalPredictor import SignalPredictor
from ApplicationSettings import Performance, SCREENSHOTS_PATH

HEADER_LENGTH = 64
PORT = 5050
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"

FLOAT_SIZE = 8

class NetworkStatus(Enum):
    WAITING = 0
    CONNECTED = 1
    RECEIVED_HEADER = 2
    RECEIVING_SIGNAL = 3
    ENDED = 4
    ERROR = 5

class DigitalSignalServer(QObject):

    network_status = pyqtSignal(NetworkStatus, str)
    ui_status = pyqtSignal(int, int)
    save_screenshot = pyqtSignal(np.ndarray, np.ndarray, np.ndarray, int)
    save_wav = pyqtSignal(int, list)

    def __init__(self, threshold = 0.5, max_lag = None):
        QObject.__init__(self)
        self.metrics_holder = MetricsHolder()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind(ADDR)
        self.connected = False
        self.lost_packets = 0
        self.anomaly_threshold = 0.0
        self.random_decision_taker = None
        self.blank_predictions = 0
        self.threshold = threshold
        self.__max_lag = max_lag
        self.x = np.array([])
        self.y = np.array([])
        self.y_copy = np.array([])


    def accept_client(self):
        self.connection, self.client_address = self.socket.accept()
        self.connected = True
        self.network_status.emit(NetworkStatus.CONNECTED, f"Client with address {self.client_address} has connected.")
        self.hasHeader = self.receive_header()
        self.network_status.emit(NetworkStatus.RECEIVED_HEADER, str(self.packets_count))

    def set_max_lag(self, max_lag: int):
        if max_lag == 0:
            self.__max_lag = None
        else:
            self.__max_lag = max_lag


    def receive_header(self):
        buffer = self.connection.recv(8)
        samples_count = struct.unpack('q', buffer)[0]

        buffer = self.connection.recv(4)
        self.sample_rate = struct.unpack('i', buffer)[0]

        buffer = self.connection.recv(4)
        self.packet_size = struct.unpack('i', buffer)[0]

        buffer = self.connection.recv(8)
        self.packets_count = struct.unpack('q', buffer)[0]

        self.packets_received = 0

        if samples_count and self.sample_rate and self.packet_size and self.packets_count:
            self.x = dsp.compute_x_axis(samples_count, self.sample_rate)
            self.predictor = SignalPredictor(self.packet_size)
            self.connection.send("OK".encode(FORMAT))
            return True
        return False
    
    def add_metrics(self, fit_time, prediction_time, mae, rmse):
        self.metrics_holder.mae_errors.append(mae)
        self.metrics_holder.rmse_errors.append(rmse)
        self.metrics_holder.fit_times.append(fit_time)
        self.metrics_holder.prediction_times.append(prediction_time)

    def receive_data(self, performance_strategy: Performance, save_screenshots):
        if self.hasHeader:
            if self.random_decision_taker is None:
                self.random_decision_taker = RandomDecisionTaker(0.0, 1.0, self.packets_count)
            if self.packets_received < self.packets_count:
                self.network_status.emit(NetworkStatus.RECEIVING_SIGNAL, f'Receiving the signal ...')
                try:
                    buffer = self.connection.recv(4)
                    current_packet_size = struct.unpack('i', buffer)[0]
                    buffer = self.connection.recv(current_packet_size)
                except ConnectionResetError:
                    self.network_status.emit(NetworkStatus.ERROR, f"The connection was forcibly closed. Data tranmitting ended.")
                    return

                next_size = int(current_packet_size / 4)
                y_data = np.frombuffer(buffer, np.float32, next_size)
                self.packets_received += 1

                is_lost = self.random_decision_taker.get_decision(self.packets_received, self.threshold)
                if  is_lost:
                    self.y_copy = np.append(self.y_copy, np.zeros(next_size))
                    self.lost_packets += 1

                    result = self.predictor.predict(self.y, self.y[-next_size:], performance_strategy, next_size, self.__max_lag, self.anomaly_threshold)
                    if result is not None:
                        predictions, fit_time, prediction_time = result
                        if len(predictions) > 0: 
                            self.y = np.append(self.y, predictions)
                            mae, rmse = compute_errors(y_data, predictions)
                            self.add_metrics(fit_time, prediction_time, mae, rmse)
                        else:
                            #self.y = np.append(self.y, np.full((next_size,), np.nan))
                            self.metrics_holder.blank_predictions += 1

                        if save_screenshots == 'true' and predictions is not None and len(predictions) > 0:
                            x = self.x[len(self.y)-next_size:len(self.y)]
                            self.save_screenshot.emit(x, y_data, predictions, self.packets_received)
                else:
                    max = np.max(y_data)
                    min = np.min(y_data)
                    if max > self.predictor.max_value:
                        self.predictor.max_value = max
                    if min < self.predictor.min_value:
                        self.predictor.min_value = min
                    self.y = np.append(self.y, y_data)
                    self.y_copy = np.append(self.y_copy, y_data)

                self.ui_status.emit(self.packets_received, next_size)
                try:
                    self.connection.send("OK".encode(FORMAT))
                except ConnectionResetError:
                    self.network_status.emit(NetworkStatus.ERROR, f"The connection was forcibly closed. Data tranmitting ended.")
                    return
            else:
                self.save_wav.emit(self.sample_rate, [self.y, self.y_copy])
                self.network_status.emit(NetworkStatus.ENDED, f'Finished. Lost {self.lost_packets} out of {self.packets_count}.')
        elif self.connected is True:
            self.network_status.emit(NetworkStatus.ERROR, 'No header data. Please try again')
            return
        
    
    def reset(self):
        self.connected = False
        self.hasHeader = False
        self.metrics_holder = MetricsHolder()
        self.lost_packets = 0
        self.random_decision_taker = None
        self.connection.close()
        self.x = np.array([])
        self.y_copy = np.array([])
        self.y = np.array([])

    def listen(self, max_connections = 1):
        self.network_status.emit(NetworkStatus.WAITING, f"Listening at {SERVER} on port {PORT} ...")
        self.socket.listen(max_connections)
        client_handler = threading.Thread(target = self.accept_client)
        client_handler.start()

        
    