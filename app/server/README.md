﻿# Instrucțiuni pentru rularea aplicației server

În primul rând este necesară existența unei distribuții de Python pe mașina folosită, având de preferat ultima versiune. Pe mașina cu ajutorul căreia a fost dezvoltată aplicația, distribuția Python are versiunea 3.7.4. Se pot urma instrucțiunile din următorul [link](https://realpython.com/installing-python/) pentru a instala o distribuție de Python.

De asemenea, este necesară existența executabilelor `python` și `pip` în variabila PATH a sistemului gazdă.

Se va deschide un terminal/command prompt, și se va naviga la directorul rădăcină al aplicației server (cel care conține sursele cu extensia `.py`).

Fișierul `requirements.txt` conține toate dependințele aplicației.
 Instalarea acestora se poate face prin comanda `pip install -r requirements.txt`. 
 Dacă apare eroarea `ERROR: Could not install packages due to an EnvironmentError: [WinError 5] Access is denied:`, atunci trebuie folosită comanda `pip install --user -r requirements.txt`.

După instalarea pachetelor, rularea aplicației se va face prin comanda `python MainWindow.py`.

