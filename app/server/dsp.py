import numpy as np

TIME_UNIT = 1

def compute_x_axis(samples_count, sample_rate):
    duration_in_seconds = samples_count / sample_rate
    increment = TIME_UNIT / sample_rate
    return np.arange(0, duration_in_seconds, increment)
