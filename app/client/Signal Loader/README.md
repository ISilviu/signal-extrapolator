﻿# Instrucțiuni pentru rularea aplicației client

Deoarece interfața aplicației este realizată folosind framework-ul WPF, aceasta poate rula doar sub sistemul de operare Windows.

Este necesar mediul de dezvoltare Visual Studio 2019 (ultima versiune este preferată). Acesta se poate instala direct de la Microsoft, de pe link-ul următor: [https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16).
La instalarea acestuia, este necesară bifarea opțiunii `.NET desktop development`.

Rularea aplicației se poate face prin câțiva pași simpli:
 1. Se navighează la directorul sursă al aplicației client (cel care conține fișierul cu extensia `.sln`).
 2. Se deschide fișierul `Signal Loader.sln` cu ajutorul Visual Studio.
 3. Se apasă combinația de taste `CTRL + F5`, care va descărca toate dependințele necesare  proiectului (este necesară conexiunea la internet) și va compila fișierele sursă. După rularea acestei comenzi, aplicația se va deschide și va fi gata de utilizare.

 
