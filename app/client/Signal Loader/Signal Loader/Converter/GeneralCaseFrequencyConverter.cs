﻿using DSP;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Signal_Loader.Converter
{
    class GeneralCaseFrequencyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is SignalType signalType)
            {
                return signalType == SignalType.Sine ? Visibility.Visible : Visibility.Hidden;
            }
            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility visibilityValue)
            {
                return visibilityValue == Visibility.Visible ? false : true;
            }
            return true;
        }
    }
}
