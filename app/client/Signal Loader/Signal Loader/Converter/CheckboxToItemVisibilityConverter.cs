﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Signal_Loader.Converter
{
    class CheckboxToItemVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isChecked)
            {
                return isChecked? Visibility.Visible : Visibility.Hidden;
            }
            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility visibilityValue)
            {
                return visibilityValue == Visibility.Visible ? false : true;
            }
            return true;
        }
    }
}
