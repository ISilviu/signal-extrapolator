﻿using DSP;
using DSP.Data;
using OxyPlot;
using OxyPlot.Series;
using Signal_Loader.Convenience;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Signal_Loader.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        private PlotModel _plotModel;
        public PlotModel PlotModel
        {
            get => _plotModel;
            set
            {
                _plotModel = value;
                OnPropertyChanged(nameof(PlotModel));
            }
        }

        private bool _isSignalSending;
        public bool IsSignalSending
        {
            get => _isSignalSending;
            set
            {
                _isSignalSending = value;
                OnPropertyChanged(nameof(IsSignalSending));
            }
        }

        private TimedMessage _message;
        public TimedMessage Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        public int[] SamplesPerPacket { get; } = new int[] { 128, 256, 512, 1024 };

        private long _packetsSent = 0;
        public long PacketsSent
        {
            get => _packetsSent;
            set
            {
                _packetsSent = value;
                OnPropertyChanged(nameof(PacketsSent));
            }
        }

        private long _totalPacketsCount = 0;
        public long TotalPacketsCount
        {
            get => _totalPacketsCount;
            set
            {
                _totalPacketsCount = value;
                OnPropertyChanged(nameof(TotalPacketsCount));
            }
        }

        private AudioData _currentSignalData;
        public AudioData CurrentSignalData
        {
            get => _currentSignalData;
            set
            {
                _currentSignalData = value;
                OnPropertyChanged(nameof(CurrentSignalData));
            }
        }


        private bool _isSignalLoaded;
        public bool IsSignalLoaded
        {
            get => _isSignalLoaded;
            set
            {
                _isSignalLoaded = value;
                OnPropertyChanged(nameof(IsSignalLoaded));
            }
        }

        private TcpClient _client;

        public MainWindowViewModel()
        {
            Message = new TimedMessage();
        }

        public async Task LoadWavAsync(string path)
        {
            try
            {
                var (samples, duration, format) = await WaveFileReader.ReadAsync(path);
                await UpdateCurrentSignalAsync(samples, format.SampleRate, Path.GetFileNameWithoutExtension(path));
            }
            catch (FileNotFoundException)
            {
                throw;
            }
            catch(NotSupportedException)
            {
                throw;
            }
        }

        private void DrawSignal(IEnumerable<DataPoint> points, string title)
        {
            var lineSeries = new LineSeries() { ItemsSource = points };
            PlotModel = new PlotModel() { Title = title };
            PlotModel.DefaultColors = new List<OxyColor>
                {
                    OxyColor.FromRgb(94, 144, 0)
                };
            PlotModel.DefaultFont = "Roboto";
            IsSignalLoaded = true;
            PlotModel.Series.Add(lineSeries);

            PlotModel.ResetAllAxes();
            PlotModel.InvalidatePlot(true);
        }

        public async Task UpdateCurrentSignalAsync(float[] samples, int sampleRate, string title)
        {
            float increment = 1.0f / sampleRate;
            float currentXValue = 0.0f;

            var points = new List<DataPoint>(samples.Length);
            await Task.Run(() =>
            {
                for (long index = 0; index < samples.Length; ++index)
                {
                    points.Add(new DataPoint(currentXValue, samples[index]));
                    currentXValue += increment;
                }
                DrawSignal(points, title);
                CurrentSignalData = new AudioData(sampleRate, samples);
            });
        }

        public async Task UpdatecurrentSignalAsync(float[] samples, float[] time, string name, int sampleRate)
        {
            Debug.Assert(samples.Length == time.Length);
            var points = new List<DataPoint>(samples.Length);
            await Task.Run(() =>
            {
                for(long index = 0; index < samples.Length; ++index)
                {
                    points.Add(new DataPoint(time[index], samples[index]));
                }
                DrawSignal(points, name);
                CurrentSignalData = new AudioData(sampleRate, samples, time);
            });
        }

        private bool ConnectToServer(string address, int port)
        {
            try
            {
                _client = new TcpClient();
                var result = _client.BeginConnect(address, port, null, null);
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(0.1));
                if(!success)
                {
                    Message.Text = "Could not connect. The connection timed out.";
                    return false;
                }
                return true;
            }
            catch(Exception e) when (e is SocketException || e is ArgumentOutOfRangeException || e is ArgumentNullException || e is TimeoutException)
            {
                Message.Text = e.Message;
                return false;
            }
        }

        private async Task<bool> SendInitialData(int packetSize, long packetsCount)
        {
            var samplesCountBytes = BitConverter.GetBytes(CurrentSignalData.SamplesCount);
            var sampleRateBytes = BitConverter.GetBytes(CurrentSignalData.SampleRate);
            var packetSizeBytes = BitConverter.GetBytes(packetSize);
            var packetsCountBytes = BitConverter.GetBytes(packetsCount);

            byte[] responseBuffer = new byte[2];
            try
            {
                var stream = _client.GetStream();
                await stream.WriteAsync(samplesCountBytes, 0, samplesCountBytes.Length);
                await stream.WriteAsync(sampleRateBytes, 0, sampleRateBytes.Length);
                await stream.WriteAsync(packetSizeBytes, 0, packetSizeBytes.Length);
                await stream.WriteAsync(packetsCountBytes, 0, packetsCountBytes.Length);

                await stream.ReadAsync(responseBuffer);
                return Encoding.UTF8.GetString(responseBuffer) == "OK";
            }
            catch (SocketException e)
            {
                Message.Text = e.InnerException.Message;
                return false;
            }
        }

        public async Task SendSignalAsync(string address, int port, int packetSize)
        {
            if(ConnectToServer(address, port))
            {
                long totalPacketsCount = CurrentSignalData.SamplesCount / packetSize;
                if (CurrentSignalData.SamplesCount % packetSize != 0)
                {
                    ++totalPacketsCount;
                }

                if(await SendInitialData(packetSize, totalPacketsCount))
                {
                    IsSignalSending = true;

                    TotalPacketsCount = totalPacketsCount;

                    Index startIndex = 0;
                    int end = packetSize;
                    Index endIndex = end;
                    for (long packetIndex = 0; packetIndex < totalPacketsCount; ++packetIndex)
                    {
                        float[] packet = CurrentSignalData.Samples[startIndex..endIndex];
                        byte[] packetBytes = new byte[packet.Length * 4];
                        Buffer.BlockCopy(packet, 0, packetBytes, 0, packetBytes.Length);

                        var stream = _client.GetStream();
                        var currentPacketSizeBytes = BitConverter.GetBytes(packetBytes.Length);
                        await stream.WriteAsync(currentPacketSizeBytes, 0, currentPacketSizeBytes.Length);
                        await stream.WriteAsync(packetBytes, 0, packetBytes.Length);

                        byte[] responseBuffer = new byte[2];
                        await stream.ReadAsync(responseBuffer);
                        if(Encoding.UTF8.GetString(responseBuffer) == "OK")
                        {
                            ++PacketsSent;
                            startIndex = endIndex;
                            end += packetSize;
                            if (end >= CurrentSignalData.Samples.Length)
                            {
                                end = CurrentSignalData.Samples.Length - 1;
                            }
                            endIndex = end;
                        }
                    }
                }
                else
                {
                    Message.Text = "The header data could not be sent. Please try again.";
                }
            }
            IsSignalSending = false;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
