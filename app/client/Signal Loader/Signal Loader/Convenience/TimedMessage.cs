﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.ComponentModel;
using System.Text;

namespace Signal_Loader.Convenience
{
    public class TimedMessage : INotifyPropertyChanged
    {
        private readonly Timer messageTimer;

        private string _text;
        public string Text
        {
            get => _text;
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    messageTimer.Start();
                }
                _text = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        private double _interval;
        public double Interval
        {
            get => _interval;
            set
            {
                _interval = value;
                messageTimer.Interval = _interval;
            }
        }

        public TimedMessage(int timeoutMiliseconds = 5000)
        {
            Text = string.Empty;
            messageTimer = new Timer();
            messageTimer.Interval = timeoutMiliseconds;
            messageTimer.Elapsed += MessageTimer_Elapsed;
        }

        private void MessageTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Text = string.Empty;
            messageTimer.Stop();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
