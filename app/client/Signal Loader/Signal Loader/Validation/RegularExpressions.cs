﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Printing;
using System.Text;
using System.Text.RegularExpressions;

namespace Signal_Loader.Validation
{
    public static class RegularExpressions
    {
        public static readonly Regex FloatingPointInputRegex = new Regex("^[^+-][0-9]*\\.?[0-9]*$");
        public static readonly Regex IntegerInputRegex = new Regex("^[1-9][0-9]{0,9}$");
        public static readonly Regex IpAddressRegex = new Regex("(^([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})$)|localhost");

        public static bool DoAllMatch(Regex regex, string[] stringsToCheck) => stringsToCheck.All(s => regex.IsMatch(s));
        public static bool DoesAnyNotMatch(Regex regex, string[] stringsToCheck) => stringsToCheck.Any(s => regex.IsMatch(s) == false);
    }
}
