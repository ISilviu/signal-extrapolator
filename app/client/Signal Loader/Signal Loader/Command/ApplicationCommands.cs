﻿using Signal_Loader.Windows;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Signal_Loader.Command
{
    public static class ApplicationCommands
    {
        public static readonly RoutedUICommand Generate = new RoutedUICommand("Generate a new signal", nameof(Generate), typeof(MainWindow),
            new InputGestureCollection()
            {
                new KeyGesture(Key.G, ModifierKeys.Control)
            }
        );

        public static readonly RoutedUICommand Import = new RoutedUICommand("Import a file", nameof(Import), typeof(MainWindow),
            new InputGestureCollection()
            {
                new KeyGesture(Key.F, ModifierKeys.Control)
            }
        );

        public static readonly RoutedUICommand GenerateNewSignal = new RoutedUICommand("Generate a new signal", nameof(GenerateNewSignal), typeof(GenerateSignalWindow),
            new InputGestureCollection()
            {
                new KeyGesture(Key.Enter)
            }
        );

        public static readonly RoutedUICommand SendSignal = new RoutedUICommand("Generate a new signal", nameof(SendSignal), typeof(MainWindow),
            new InputGestureCollection()
            {
                new KeyGesture(Key.Enter, ModifierKeys.Control)
            }
        );
    }
}
