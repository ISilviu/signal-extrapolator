﻿using DSP;
using Signal_Loader.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Re = Signal_Loader.Validation.RegularExpressions;

namespace Signal_Loader.Windows
{
    public partial class GenerateSignalWindow : Window
    {
        private readonly Dictionary<SignalType, Func<bool>> SignalTypeToCheckFunction = new Dictionary<SignalType, Func<bool>>();

        private static readonly IReadOnlyDictionary<SignalType, string> SignalTypeToString = new Dictionary<SignalType, string>()
        {
            { SignalType.Sine, "Sine" },
            { SignalType.SineSum, "Sine Sum" },
            { SignalType.Sweep, "Sweep" }
        };

        private static readonly int EmptyArraySize = 0;

        public GenerateSignalWindow()
        {
            InitializeComponent();
        }

        private void GenerateSignalWindow_Loaded(object sender, RoutedEventArgs e)
        {
            SignalTypeComboBox.ItemsSource = Enum.GetValues(typeof(SignalType)).Cast<SignalType>();
            SignalTypeComboBox.SelectedItem = SignalType.Sine;

            SignalTypeToCheckFunction.Add(SignalType.Sine, () =>
                Re.FloatingPointInputRegex.IsMatch(CenterAmplitudeTextBox.Text) &&
                Re.FloatingPointInputRegex.IsMatch(CenterFrequencyTextBox.Text) 
            );

            SignalTypeToCheckFunction.Add(SignalType.SineSum, () =>
                Re.FloatingPointInputRegex.IsMatch(LeftAmplitudeTextBox.Text) &&
                Re.FloatingPointInputRegex.IsMatch(RightAmplitudeTextBox.Text) &&
                Re.FloatingPointInputRegex.IsMatch(LeftFrequencyTextBox.Text) &&
                Re.FloatingPointInputRegex.IsMatch(RightFrequencyTextBox.Text)
            );

            SignalTypeToCheckFunction.Add(SignalType.Sweep, () =>
               Re.FloatingPointInputRegex.IsMatch(CenterAmplitudeTextBox.Text) &&
               Re.FloatingPointInputRegex.IsMatch(LeftFrequencyTextBox.Text) &&
               Re.FloatingPointInputRegex.IsMatch(RightFrequencyTextBox.Text)
            );
        }

        private void GenerateNewSignal_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if(SignalTypeComboBox?.SelectedItem != null)
            {
                var signalType = (SignalType)SignalTypeComboBox.SelectedItem;
                if (SignalTypeToCheckFunction.ContainsKey(signalType))
                {
                    if(SignalTypeToCheckFunction[signalType]() && Re.IntegerInputRegex.IsMatch(SampleRateTextBox.Text) && Re.FloatingPointInputRegex.IsMatch(DurationTextBox.Text))
                    {
                        if(NoiseCheckBox.IsChecked == true)
                        {
                            if(Re.FloatingPointInputRegex.IsMatch(MeanTextBox.Text) && Re.FloatingPointInputRegex.IsMatch(StandardDeviationTextBox.Text))
                            {
                                e.CanExecute = true;
                            }
                            else
                            {
                                e.CanExecute = false;
                            }
                        }
                        else
                        {
                            e.CanExecute = true;
                        }
                    }
                    else
                    {
                        e.CanExecute = false;
                    }
                }
            }
        }

        private async void GenerateNewSignal_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if(DataContext is MainWindowViewModel viewModel)
            {
                float[] samples = new float[EmptyArraySize];
                float[] time = new float[EmptyArraySize];

                int sampleRate = int.Parse(SampleRateTextBox.Text);
                double duration = double.Parse(DurationTextBox.Text, CultureInfo.InvariantCulture);

                double mean = 0.0;
                double std = 0.0;
                if(NoiseCheckBox.IsChecked == true)
                {
                    mean = double.Parse(MeanTextBox.Text);
                    std = double.Parse(StandardDeviationTextBox.Text);
                }
                var signalType = (SignalType)SignalTypeComboBox.SelectedItem;

                switch (signalType)
                {
                    case SignalType.Sine:
                        {
                            var (xAxis, yAxis) = SignalGenerator.Generate(
                                sampleRate, 
                                double.Parse(CenterAmplitudeTextBox.Text),
                                double.Parse(CenterFrequencyTextBox.Text), 
                                duration,
                                mean,
                                std);

                            samples = yAxis;
                            time = xAxis;
                            break;
                        }                        
                    case SignalType.SineSum:
                        {
                            var (xAxis, yAxis) = SignalGenerator.Generate(
                                sampleRate,
                                double.Parse(LeftAmplitudeTextBox.Text),
                                double.Parse(RightAmplitudeTextBox.Text),
                                double.Parse(LeftFrequencyTextBox.Text),
                                double.Parse(RightFrequencyTextBox.Text), 
                                duration,
                                mean,
                                std);

                            samples = yAxis;
                            time = xAxis;
                            break;
                        }
                    case SignalType.Sweep:
                        {
                            var (xAxis, yAxis) = SignalGenerator.Generate(
                                sampleRate, 
                                double.Parse(CenterAmplitudeTextBox.Text),
                                double.Parse(LeftFrequencyTextBox.Text),
                                double.Parse(RightFrequencyTextBox.Text), 
                                duration,
                                mean,
                                std);

                            samples = yAxis;
                            time = xAxis;
                            break;
                        }
                }

                if(SignalTypeToString.ContainsKey(signalType))
                {
                    await viewModel.UpdatecurrentSignalAsync(samples, time, SignalTypeToString[signalType], sampleRate);
                }
                else
                {
                    await viewModel.UpdatecurrentSignalAsync(samples, time, signalType.ToString(), sampleRate);
                }

                Close();
            }
            
        }
    }
}
