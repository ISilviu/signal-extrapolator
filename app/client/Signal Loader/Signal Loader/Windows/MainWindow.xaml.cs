﻿using Microsoft.Win32;
using Signal_Loader.Convenience;
using Signal_Loader.ViewModel;
using Signal_Loader.Windows;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Input;
using Re = Signal_Loader.Validation.RegularExpressions;

namespace Signal_Loader
{
    public partial class MainWindow : Window
    {
        private static readonly string DefaultAudioFileExtension = ".wav";
        private static readonly string WavFilesFilter = "WAV files (*.wav)|*.wav";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void GenerateCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (DataContext is MainWindowViewModel viewModel)
            {
                var generateSignalWindow = new GenerateSignalWindow();
                generateSignalWindow.DataContext = viewModel;
                generateSignalWindow.ShowDialog();
            }
        }

        private async void ImportCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var fileDialog = new OpenFileDialog()
            {
                DefaultExt = DefaultAudioFileExtension,
                Title = "Import WAV file",
                Filter = WavFilesFilter
            };

            if(fileDialog.ShowDialog() == true)
            {
                if(DataContext is MainWindowViewModel viewModel)
                {
                    try
                    {
                        await viewModel.LoadWavAsync(fileDialog.FileName);
                    }
                    catch (FileNotFoundException ex)
                    {
                        viewModel.Message.Text = ex.Message;
                    }
                    catch(System.ArgumentException ex)
                    {
                        viewModel.Message.Text = ex.Message;
                    }
                    catch(NotSupportedException ex)
                    {
                        viewModel.Message.Text = ex.Message;
                    }
                }
            }
        }

        private async void SendSignal_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if(DataContext is MainWindowViewModel viewModel)
            {
                try
                {
                    await viewModel.SendSignalAsync(ServerAddressTextBox.Text, int.Parse(ServerPortTextBox.Text), (int)PacketSizeComboBox.SelectedItem);
                }
                catch (IOException ex)
                {
                    viewModel.Message.Text = ex.Message;
                    viewModel.IsSignalSending = false;
                }
                catch(InvalidOperationException ex)
                {
                    viewModel.Message.Text = ex.InnerException.Message;
                    viewModel.IsSignalSending = false;
                }
            }
        }


        private void ChangeCurrentSignal_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (DataContext is MainWindowViewModel viewModel)
            {
                e.CanExecute = !viewModel.IsSignalSending;
            }
        }

        private void SendSignal_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (DataContext is MainWindowViewModel viewModel)
            {
                var stringsToCheck = new string[] { ServerPortTextBox.Text };
                e.CanExecute =
                   viewModel.IsSignalLoaded &&
                   !string.IsNullOrEmpty(ServerAddressTextBox.Text) && 
                   Re.IpAddressRegex.IsMatch(ServerAddressTextBox.Text) &&
                   PacketSizeComboBox.SelectedItem != null &&
                  !viewModel.IsSignalSending &&
                   Re.DoAllMatch(Re.IntegerInputRegex, stringsToCheck);
            }
        }
    }
}
