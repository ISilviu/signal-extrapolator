﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSP
{
    public enum SignalType
    {
        Sine,
        SineSum,
        Sweep
    }
}
