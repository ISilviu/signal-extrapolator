﻿using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DSP
{
    public static class WaveFileReader
    {
        public static Task<(float[], TimeSpan, WaveFormat)> ReadAsync(string path)
        {
            CheckPath(path);
            return Task.Run(() => ReadWav(path));
        }

        public static (float[], TimeSpan, WaveFormat) Read(string path)
        {
            CheckPath(path);
            return ReadWav(path);
        }

        private static void CheckPath(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("File not found.");
            }
        }

        private static (float[], TimeSpan, WaveFormat) ReadWav(string path)
        {
            using (var waveFileReader = new NAudio.Wave.WaveFileReader(path))
            {
                if(waveFileReader.WaveFormat.Channels > 1)
                {
                    throw new NotSupportedException("Only mono wav files are supported for now.");
                }

                var buffer = new float[waveFileReader.SampleCount];
                try
                {
                   waveFileReader.ToSampleProvider().Read(buffer, 0, buffer.Length);
                }
                catch (ArgumentException)
                {
                    throw;
                }
                return (buffer, waveFileReader.TotalTime, waveFileReader.WaveFormat);
            }
        }
    }
}
