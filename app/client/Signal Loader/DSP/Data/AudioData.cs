﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace DSP.Data
{
    public struct AudioData
    {
        public int SampleRate { get; set; }
        public long SamplesCount { get; set; }
        public float[] Samples { get; set; }

        public float[] Time { get; set; }

        public AudioData(int sampleRate, float[] samples)
        {
            SampleRate = sampleRate;
            Samples = samples;
            SamplesCount = samples.LongLength;

            Time = new float[SamplesCount];
            ComputeXAxis();
        }

        public AudioData(int sampleRate, float[] samples, float[] time)
        {
            Debug.Assert(samples.LongLength == time.LongLength);
            SampleRate = sampleRate;
            Samples = samples;
            Time = time;
            SamplesCount = samples.LongLength;
        }

        private void ComputeXAxis()
        {
            float increment = 1.0f / SampleRate;
            float currentXValue = 0.0f;

            for (long index = 0; index < SamplesCount; ++index)
            {
                Time[index] = currentXValue;
                currentXValue += increment;
            }
        }
    }
}
