﻿using CenterSpace.NMath.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace DSP
{
    public static class SignalGenerator
    {

        private static FloatVector GetRandomSpikes(int count, double mean, double std)
        {
            var normalRandomNumberGenerator = new RandGenNormal(mean, std);
            return new FloatVector(count, normalRandomNumberGenerator);
        }

        public static (float[], float[]) Generate(int sampleRate, double amplitude, double frequency, double duration, double mean, double std )
        {
            long totalSamplesCount = (long)Math.Ceiling(sampleRate * duration);
            float increment = 1.0f / sampleRate;
            var xAxis = new float[totalSamplesCount];
            var yAxis = new float[totalSamplesCount];

            float currentValue = 0.0f;
            FloatVector randomSpikes = GetRandomSpikes((int)totalSamplesCount, mean, std);

            for (long index = 0; index < totalSamplesCount; ++index)
            {
                xAxis[index] = currentValue;
                yAxis[index] = (float)(amplitude * Math.Sin(2 * Math.PI * frequency * currentValue));

                yAxis[index] += randomSpikes[(int)index];
                
                currentValue += increment;
            }
            return (xAxis, yAxis);
        }

        public static (float[], float[]) Generate(int sampleRate, double amplitude1, double amplitude2, double frequency1, double frequency2, double duration, double mean , double std)
        {
            long totalSamplesCount = (long)Math.Ceiling(sampleRate * duration);
            float increment = 1.0f / sampleRate;
            var xAxis = new float[totalSamplesCount];
            var yAxis = new float[totalSamplesCount];

            float currentValue = 0.0f;
            FloatVector randomSpikes = GetRandomSpikes((int)totalSamplesCount, mean, std);

            for (long index = 0; index < totalSamplesCount; ++index)
            {
                xAxis[index] = currentValue;

                float firstSine =  (float)(amplitude1 * Math.Sin(2 * Math.PI * frequency1 * currentValue));
                float secondSine = (float)(amplitude2 * Math.Sin(2 * Math.PI * frequency2 * currentValue));
                yAxis[index] = firstSine + secondSine;

                yAxis[index] += randomSpikes[(int)index];

                currentValue += increment;
            }

            return (xAxis, yAxis);
        }

        public static (float[], float[]) Generate(int sampleRate, double amplitude, double frequency1, double frequency2, double duration, double mean, double std)
        {
            long totalSamplesCount = (long)Math.Ceiling(sampleRate * duration);
            float increment = 1.0f / sampleRate;
            var xAxis = new float[totalSamplesCount];
            var yAxis = new float[totalSamplesCount];

            float phaseAccumulator = 0.0f;
            float initialFrequency = 0.0f;
            float delta = 2 * (float)Math.PI * initialFrequency / sampleRate;
            float deltaDiff = (float)((frequency2 - frequency1) / (sampleRate * duration));

            float currentValue = 0.0f;
            FloatVector randomSpikes = GetRandomSpikes((int)totalSamplesCount, mean, std);
            for (long index = 0; index < totalSamplesCount; ++index)
            {
                xAxis[index] = currentValue;

                float currentYValue = (float)(amplitude * Math.Sin(phaseAccumulator));
                if (currentYValue > amplitude)
                    currentYValue = (float)amplitude;
                if (currentYValue < -amplitude)
                    currentYValue = (float)-amplitude;

                yAxis[index] = currentYValue;
                phaseAccumulator += delta;
                initialFrequency += deltaDiff;
                delta = (float)(2 * Math.PI * initialFrequency / sampleRate);

                yAxis[index] += randomSpikes[(int)index];

                currentValue += increment;
            }
            return (xAxis, yAxis);
        }
    }
}
