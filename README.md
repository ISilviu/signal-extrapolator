﻿# Digital signal extrapolator	
This application has been developed as my thesis project. 

# The problem
It attempts to solve the problem of losing packets while transmitting digital signals across the network. Suppose there is a system having 2 components:
- a recorder that captures sounds from a microphone and stores them internally
- an analyzer that will have to calculate different metrics of the signal acquired by the recorder. For the analyzer to have access to the signal in a rapid manner, the recorder will split the sound in multiple packets and send it over the network using UDP.
![enter image description here](https://i.postimg.cc/2j2jBvyG/signal1.png)

While UDP is faster than TCP, it doesn't guarantee that all the packets arrive to the target. As the above scenario has to run in a time limited scenario, changing from TCP to UDP is not an option. 

That is, the problem is that some packets are lost during the transmission and the analyzer needs them all in order to provide correct results.

# The solution

The solution stands for an extrapolation algorithm that will be able to recover the lost packet by predicting it using the already received packets.
![enter image description here](https://i.postimg.cc/J0Ghh4NM/signal2.png)

As the running time is limited, classic machine-learning algorithms were favored over their deep-learning counterparts. 

Various research was done in the field of autoregressive algorithms like AR, ARMA, exponential smoothing.

# The application
 It simply simulates the scenario described in the second section. It uses a simple client-server architecture. The client acts as the recorder, while the server acts as the analyzer. 

TCP was used to have a full control over the simulation. At random occasions, the server will simulate the loss of a certain packet. In that case, it will try to predict it using a ML algorithm.

![enter image description here](https://i.postimg.cc/8kXsj51q/signal6.gif)

# Technical aspects

The client application is implemented in C# and WPF. It uses NAudio to interpret audio files and OxyPlot to draw the signal in the display. 

The server application is implemented in Python and PyQt5. It uses various 3rd party libraries:
- statsmodels for the ML algorithms
- scipy to compute different statistics used in the research
- librosa to write the received signal to the disk

# Disclaimer
The best results can be achieved on artificial signals. On real sounds, it really depends on the complexity of that sound. This thesis was centered on research, and further research is needed to obtain better results also for more complex signals.







